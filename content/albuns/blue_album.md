---
title: "Blue Album"
date: 2022-10-09T14:47:00-03:00
draft: false
---

![album](/meusalbuns/image/Weezer_-_Blue_Album.png)

Weezer, também referido como The Blue Album, é o álbum de estreia da banda americana de rock alternativo Weezer, lançado em Maio de 1994 pela DGC Records. O álbum foi produzido pelo antigo vocalista da banda The Cars, Ric Ocasek, e gravado no Electric Lady Studios em Nova Iorque. O The Blue Album gerou singles populares como "Undone – The Sweater Song", "Buddy Holly" e "Say It Ain't So", os quais são responsáveis por lançar os Weezer ao sucesso no panorama musical com a ajuda dos vídeos musicais realizados por Spike Jonze. Estima-se que o álbum tinha vendido 3,300,000 cópias nos Estados Unidos, no qual atingiu o número 16 na tabela Billboard 200. O álbum encontra-se certificado com tripla platina nos Estados Unidos e dupla platina no Canadá.

Fonte: [Wikipedia](https://en.wikipedia.org/wiki/Weezer_(Blue_Album))