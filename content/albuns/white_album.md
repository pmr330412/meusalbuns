---
title: "White Album"
date: 2022-10-09T14:47:00-03:00
draft: false
---

![album](/meusalbuns/image/TheBeatles68LP.jpg)

The Beatles, também conhecido como "O Álbum Branco" é o décimo álbum de estúdio dos Beatles, lançado como disco duplo em 22 de novembro de 1968. Um álbum duplo, com uma capa branca com nada nela, apenas o nome da banda em alto relevo, que foi feito com a ideia de ter um contraste com a arte de capa de seu álbum anterior Sgt. Pepper's Lonely Hearts Club Band, mesmo que nenhum single tenha sido posto no album na Grã-Bretanha e nos Estados Unidos, as músicas "Hey Jude" e "Revolution" se originaram das mesmas gravações, e ambos foram lançados em um compacto, em Agosto de 1968. As músicas do álbum tem um repertório muito diferenciado em estilos, indo do blues britânico para o Ska, com pastiches de Chuck Berry e Karlheinz Sotckhausen.

Muita das músicas no album foram escritas durante Março e Abril de 1968 na viagem de Meditação Transcendental feita pelos Beatles para Rishikesh, na Índia. O grupo voltou para os Estúdios da Abbey Road em Londres no fim de Maio para começar as sessões de gravação que durariam até metade de Outubro. Durante essas sessões, discussões por diferenças criativas eram frequente entre os membros da banda. Outro decisivo elemento era a constante presença da parceira de John Lennon, Yoko Ono, que quebrava a politica do grupo de que namoradas e esposas não poderiam comparecer nas gravações. Depois de uma série de problemas, incluindo o produtor George Martin indo embora do nada e o engenheiro desaparecendo sem dar resposta, Ringo Starr até chegou a deixar a banda em Agosto. As mesmas tensões continuaram pelo ano seguinte, até o fim da banda em 1970.

Em lançamento, The Beatles teve um recebimento muito bom em meio a critica, mas outros comentadores acharam músicas sarcásticas, sem importância e apolíticas, devido ao turbulento estado social e político de 1968. A banda e Martin debateram mais tarde se eles deveriam ter lançado um album único, ao invés de um duplo. Mesmo assim, The Beatles chegou em número 1 nas paradas tanto nos Estados Unidos quanto no Reino Unido, e tem sido considerado por muitas criticas especializadas, como um dos melhores álbuns de todos os tempos, estando na lista dos 200 álbuns definitivos no Rock and Roll Hall of Fame.

Em 1997, O Álbum Branco foi nomeado o décimo melhor disco de todos os tempos pela "Music of the Millennium" da Classic FM. Em 1998 a Q Magazine colocou como 17° lugar e em 2000 em 7° lugar. A Rolling Stone colocou como o décimo entre 500 álbuns e o canal VH1 como 11° lugar. De acordo com a Associação da Indústria de Discos da América, o disco é 19 vezes disco de platina e o décimo disco mais vendido nos Estados Unidos.

Em 2010, um colecionador argentino possuía o álbum com assinaturas originais dos quatro Beatles. A peça foi vendida na ocasião por 33 mil dólares.

Fonte: [Wikipedia](https://pt.wikipedia.org/wiki/The_Beatles_(%C3%A1lbum))