---
title: "Black Album"
date: 2022-10-09T14:47:00-03:00
draft: false
---

![album](/meusalbuns/image/Metallica_álbum.jpg)

Metallica, também conhecido como The Black Album ("O Álbum Negro"), é o quinto álbum de estúdio da banda norte-americana de heavy metal Metallica, lançado a 12 de agosto de 1991.

Com faixas como "The Unforgiven", "Enter Sandman" e "Nothing Else Matters", tornou-se no álbum de maior sucesso do grupo, tendo vendido cerca de 16.040.000 de cópias apenas nos Estados Unidos, sendo o álbum que mais vendeu no país desde que a Billboard começou a usar a Nielsen SoundScan para calcular as vendas, e mais de 40 milhões mundialmente. Este álbum está na lista dos 200 álbuns definitivos no Rock and Roll Hall of Fame, ficando em 14º lugar. Em 1992, Metallica ganhou o Grammy Award por Melhor performance de Heavy Metal Em 2003, ficou em 252º na lista dos 500 melhores álbuns de sempre da Revista Rolling Stone e em 25º na lista de melhores álbuns de metal de todos os tempos da mesma publicação.

A capa do álbum tem somente o logotipo da banda e uma cobra enrolada, derivada da bandeira de Gadsden. O lema da bandeira de Gadsden, "Don't Tread on Me", é usado como título de uma das músicas do álbum. Em 2004, foi lançado um DVD-Audio com uma mixagem 5.1 do álbum.

Fonte: [Wikipedia](https://pt.wikipedia.org/wiki/Metallica_(%C3%A1lbum))