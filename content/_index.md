Neste site, você encontrará

- Meus álbuns
- Detalhes sobre eles

---

# Lista de álbuns

[White Album](/meusalbuns/albuns/white_album/)

![album](image/TheBeatles68LP.jpg)

---
[Black Album](/meusalbuns/albuns/black_album/)

![album](image/Metallica_álbum.jpg)

---
[Blue Album](/meusalbuns/albuns/blue_album/)

![album](image/Weezer_-_Blue_Album.png)

---